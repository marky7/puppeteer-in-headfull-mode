FROM node:latest
WORKDIR /puppeteer

ENV NODE_ENV=production
ENV DEBIAN_FRONTEND=noninteractive 

# Etc/UTC+1 -> UTC +01:00 timezone offset
ENV TZ=Etc/UTC

# The dependencies are being installed manually to facilitate use of the Chromium binary that’s bundled with Puppeteer. 
# This ensures consistency between Puppeteer releases and avoids the possibilities of a new Chrome release arriving with incompatibilities that break Puppeteer.

RUN apt-get update && \ 
    apt-get install -y \
    apt-utils \
    fonts-liberation \
    gconf-service \
    libasound2 \
    libatk1.0-0 \
    libcairo2 \
    libcups2 \
    libfontconfig1 \
    libgbm-dev \
    libgdk-pixbuf2.0-0 \
    libgtk-3-0 \
    libicu-dev \
    libjpeg-dev \
    libnspr4 \
    libnss3 \
    libpango-1.0-0 \
    libpangocairo-1.0-0 \
    libpng-dev \
    libx11-6 \
    libx11-xcb1 \
    libxcb1 \
    libxcomposite1 \
    libxcursor1 \
    libxdamage1 \
    libxext6 \
    libxfixes3 \
    libxi6 \
    libxrandr2 \
    libxrender1 \
    libxss1 \
    libxtst6 \
    xdg-utils

# Installing display server implementing the X11 display server protocol
RUN apt-get install -y \
    xvfb

# Install the puppeteer project in headfull mode
RUN mkdir app
COPY /app ./app
COPY /scripts ./scripts
RUN cd app && npm install

# The final step is to make Puppeteer’s bundled Chromium binary properly executable. 
# Otherwise, you’ll run into permission errors whenever Puppeteer tries to start Chromium.
RUN cd app && chmod -R o+rwx ./node_modules/puppeteer/.local-chromium

# The environement will define the behaviour. Check package.json file.
CMD cd scripts && bash ./start_in_docker.sh

# NOTE : You might want to manually install a specific Chromium version in customized environments. 
# NOTE : Setting the PUPPETEER_SKIP_CHROMIUM_DOWNLOAD environment variable before you run npm install will disable Puppeteer’s own browser download during installation. This helps slim down your final image.


