#!/bin/bash

DIR="$(dirname "$(realpath "$0")")"
cd $DIR/..

docker build . -t puppeteer-headfull-app:latest
docker image ls
sudo docker run -v "$(pwd)"/app/output:/puppeteer/app/output puppeteer-headfull-app