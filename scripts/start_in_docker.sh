#!/bin/bash

DIR="$(dirname "$(realpath "$0")")"
cd $DIR/../app/

if [[ "${NODE_ENV}" = "production" ]]
then
  echo "The environment is production."
  cd ../app/
  xvfb-run --server-args="-screen 0 2560x1440x24" node index.js
else
  export NODE_ENV=development;
  echo "The environment is development."
  sleep infinity;
fi