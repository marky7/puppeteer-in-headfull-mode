const puppeteer = require("puppeteer");
var fs = require('fs');
const path = require('path');
const outputPath = path.resolve(__dirname+"/output");
/* 

This demonstrates a simple script that launches a headless Chrome instance, navigates to a URL, and captures a screenshot of the page. The browser is then closed to avoid wasting system resources.

The important section is the arguments list that’s passed to Chromium as part of the launch() call:

- disable-gpu – The GPU isn’t usually available inside a Docker container, unless you’ve specially configured the host. Setting this flag explicitly instructs Chrome not to try and use GPU-based rendering.
- no-sandbox and disable-setuid-sandbox – These disable Chrome’s sandboxing, a step which is required when running as the root user (the default in a Docker container). Using these flags could allow malicious web content to escape the browser process and compromise the host. It’s vital you ensure your Docker containers are strongly isolated from your host. If you’re uncomfortable with this, you’ll need to manually configure working Chrome sandboxing, which is a more involved process.
- disable-dev-shm-usage – This flag is necessary to avoid running into issues with Docker’s default low shared memory space of 64MB. Chrome will write into /tmp instead.

*/

(async () => {

    createOutputFolder();

    const browser = await puppeteer.launch({
        headless: false,
        args: [
            "--disable-gpu",
            "--disable-dev-shm-usage",
            "--disable-setuid-sandbox",
            "--no-sandbox",
        ]
    });
    
    const page = await browser.newPage();
    await page.setViewport({ width: 2560, height: 1440}); // WQHD screen resolution
    await page.goto("https://www.google.com/");

    console.log(outputPath);
    await page.waitForTimeout(10000); // The page can make few seconds to be fully displayed
    const ss = await page.screenshot({path: path.resolve(outputPath+"/screenshot.png")});

    await page.close();
    await browser.close();    
})();

function createOutputFolder(){
    if (!fs.existsSync(outputPath)){
        fs.mkdirSync(outputPath);
    }
}
