```
██████  ██    ██ ██████  ██████  ███████ ████████ ███████ ███████ ██████  
██   ██ ██    ██ ██   ██ ██   ██ ██         ██    ██      ██      ██   ██ 
██████  ██    ██ ██████  ██████  █████      ██    █████   █████   ██████  
██      ██    ██ ██      ██      ██         ██    ██      ██      ██   ██ 
██       ██████  ██      ██      ███████    ██    ███████ ███████ ██   ██
```

# Puppeteer in headfull mode

This project aims to create a Docker image that uses Puppeteer to execute code inside Chromium (headfull mode).

A simple nodejs application with Puppeteer in headfull mode was created as a proof of concept. The application takes a screenshot of a web page and stores it in the output folder at the root of the project.

# Default app description

Once started, the app will open a browser on the google home page.
Puppeteer will take a screenshot and create it into the "output" folder located inside app folder.

## Usage

### Start the app in local
```
cd app
npm install
npm start
```

### Build docker image for development mode
```
cd scripts
bash build_and_run_dev.sh
```
Go into the container"
```
cd app && npm run start_in_docker
```

### Build docker image for production mode
```
cd scripts
bash build_and_run_prod.sh
```

## License

Licensed under the Apache License, Version 2.0.
